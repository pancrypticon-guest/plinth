#!/usr/bin/python3
#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Configuration helper for Cockpit.
"""

import argparse

from plinth import action_utils
from plinth.modules.cockpit import utils


def parse_arguments():
    """Return parsed command line arguments as dictionary."""
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='subcommand', help='Sub command')

    subparser = subparsers.add_parser('setup',
                                      help='Setup Cockpit configuration')
    subparser.add_argument('domain_names', nargs='*',
                           help='Domain names to be allowed')
    subparser = subparsers.add_parser(
        'add-domain',
        help='Allow a new domain to be origin for Cockpit\'s WebSocket')
    subparser.add_argument('domain_name', help='Domain name to be allowed')
    subparser = subparsers.add_parser(
        'remove-domain',
        help='Disallow a new domain from being origin for Cockpit\'s '
        'WebSocket')
    subparser.add_argument('domain_name', help='Domain name to be removed')

    subparsers.required = True
    return parser.parse_args()


def subcommand_setup(arguments):
    """Setup Cockpit configuration."""
    aug = utils.load_augeas()
    origins = [
        utils.get_origin_from_domain(domain)
        for domain in arguments.domain_names
    ]
    origins += ['https://localhost', 'https://localhost:4430']
    _set_origin_domains(aug, origins)
    aug.set('/files' + utils.CONFIG_FILE + '/WebService/UrlRoot', '/_cockpit/')
    aug.save()

    action_utils.service_restart('cockpit.socket')


def _set_origin_domains(aug, origins):
    """Set the list of allowed origin domains."""
    aug.set('/files' + utils.CONFIG_FILE + '/WebService/Origins',
            ' '.join(origins))


def subcommand_add_domain(arguments):
    """Allow a new domain to be origin for Cockpit's WebSocket."""
    aug = utils.load_augeas()
    origins = utils.get_origin_domains(aug)
    origins.add(utils.get_origin_from_domain(arguments.domain_name))
    _set_origin_domains(aug, origins)
    aug.save()


def subcommand_remove_domain(arguments):
    """Disallow a domain from being origin for Cockpit's WebSocket."""
    aug = utils.load_augeas()
    origins = utils.get_origin_domains(aug)
    try:
        origins.remove(utils.get_origin_from_domain(arguments.domain_name))
    except KeyError:
        pass
    else:
        _set_origin_domains(aug, origins)
        aug.save()


def main():
    """Parse arguments and perform all duties."""
    arguments = parse_arguments()

    subcommand = arguments.subcommand.replace('-', '_')
    subcommand_method = globals()['subcommand_' + subcommand]
    subcommand_method(arguments)


if __name__ == '__main__':
    main()
