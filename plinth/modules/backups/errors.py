#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from plinth.errors import PlinthError


class BorgError(PlinthError):
    """Generic borg errors"""


class BorgRepositoryDoesNotExistError(BorgError):
    """Borg access to a repository works but the repository does not exist"""


class SshfsError(PlinthError):
    """Generic sshfs errors"""


class BorgRepositoryExists(BorgError):
    """A repository at target location already exists during initialization."""


class BorgUnencryptedRepository(BorgError):
    """Attempt to provide password on an unencrypted repository."""
